import 'dart:ui';

class Palette {
  static Color primary1 = Color(0xff1F3C88);
  static Color primary2 = Color(0xffFF6A0D);
  static Color primary3 = Color(0xff393B44);
  static Color primary4 = Color(0xffEFF0F0);
  static Color primary5 = Color(0xffEAEAEA);

  static Color hijauTua = Color(0xff006838);
  static Color hijau = Color(0xff00A451);
  static Color oren = Color(0xffF5921D);
  static Color merah = Color(0xffED1C24);
  static Color biruMuda = Color(0xff00AEEF);
  static Color biruToska = Color(0xff04C1A8);
  static Color biru = Color(0xff0637C0);
  static Color coklat = Color(0xff988279);
  static Color crem = Color(0xffE4E5E6);

  static Color bgcolor = Color(0xffF5F6F7);
  static Color bgcolorfield = Color(0xffF7F8F9);

  static var bordercolor = Color(0xffE7EBEF);

  static var denomNoActive = Color(0xffD4D4D4);

  static var labelgrey = Color(0xff3C4655);

  static var bglabelmoney = Color(0xffFAB495);

  static var shadowbox = Color(0xff111D532E);

  static var loadingcolor = Color(0xff212121);

  static var divider = Color(0xffCED2D5);

  static Color textColors = Color(0xff444445);

  static List<Color> kitGradients = [Color(0xff423144), Color(0xff8f0000)];

}
