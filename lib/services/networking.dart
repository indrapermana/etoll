import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

class NetworkHelper {
  NetworkHelper(this.url, this.jsonInput, this.token);

  final String url;
  final dynamic jsonInput;
  final String token;
  final _storage = FlutterSecureStorage();

  Future postForLogin() async {
    Map<String, String> headersGet = header();

    DataConnectionChecker().addresses = List.unmodifiable([
      AddressCheckOptions(
        InternetAddress('1.1.1.1'),
        port: DataConnectionChecker.DEFAULT_PORT,
        timeout: DataConnectionChecker.DEFAULT_TIMEOUT,
      ),
      AddressCheckOptions(
        InternetAddress('8.8.4.4'),
        port: DataConnectionChecker.DEFAULT_PORT,
        timeout: DataConnectionChecker.DEFAULT_TIMEOUT,
      ),
      AddressCheckOptions(
        InternetAddress('208.67.222.222'),
        port: DataConnectionChecker.DEFAULT_PORT,
        timeout: DataConnectionChecker.DEFAULT_TIMEOUT,
      ),
      AddressCheckOptions(
        InternetAddress('10.62.176.55'),
        port: DataConnectionChecker.DEFAULT_PORT,
        timeout: DataConnectionChecker.DEFAULT_TIMEOUT,
      ),
    ]);
    print('status koneksi');
//    print(await DataConnectionChecker().hasConnection);
//    bool statconn = await DataConnectionChecker().hasConnection;

//    if (statconn) {
      try {
        print(url);
        print(headersGet);
        print(jsonInput);

        http.Response response = await http.post(
          url,
          // headers: headersGet,
          body: jsonInput,
        );

        var decodedData = jsonDecode(response.body);

        print('ResponseAPI : ' + response.body);

        return decodedData;
      } catch (e) {
        print(e.toString());
        //return e.toString();
        // return "Error Response : Check Your Network Connection !";
        var response = {
          "status" : 500,
          "msg": "Error Response : Check Your Network Connection !",
          "result": null
        };
        return response;
      }
//    } else {
//      print('Tidak Terhubung Dengan Server, Cek jaringan dan data');
//      var response = {
//        "status" : 500,
//        "msg": "Error Response : Check Your Network Connection !",
//        "result": null
//      };
//      return response;
//    }
  }

  Future postRequest() async {
    Map<String, String> headersGet = await standardHeader();

//    bool statconn = await DataConnectionChecker().hasConnection;

//    if (statconn) {
      try {
        // print(serverUrl + url);
        print("POST");
        print(url);
        print(headersGet);
        print(jsonInput);

        http.Response response;
        response = await http.post(
          url,
          headers: headersGet,
          body: jsonInput,
        );
        // print(response.body);
        var decodedData = jsonDecode(response.body);

        print(decodedData);

        return decodedData;
      } catch (e) {
        print(e);
        var response = {
          "status" : 500,
          "msg": "Error Response : Check Your Network Connection !",
          "result": null
        };
        return response;
      }
//    } else {
//      print('Tidak Terhubung Dengan Server, Cek jaringan dan data');
//
//      var response = {
//        "status" : 500,
//        "msg": "Error Response : Check Your Network Connection !",
//        "result": null
//      };
//      return response;
//    }
  }

  Future getRequest({String msg}) async {
    Map<String, String> headersGet = await standardHeader();
//    bool statconn = await DataConnectionChecker().hasConnection;

//    if (statconn) {
      try {
        // print(serverUrl + url);
        print("GET");
        print(headersGet);

        http.Response responseget;
        print(url);
        responseget = await http.get(
          url,
          headers: headersGet,
        );

        // if (responseget.statusCode == 200) {
          String data = responseget.body;
          var decodedData = jsonDecode(data);
          
          print(responseget.statusCode);
          // print(responseget.body);
          if(msg!=null){
            print("$msg : ${responseget.body}");
          } else {
            print(responseget.body);
          }
          return decodedData;
        // } else {
          // print(responseget.statusCode);
          // print(responseget.body);
        // }
      } catch (e) {
        print(e);
        var response = {
          "status" : 500,
          "msg": "Error Response : Check Your Network Connection !",
          "result": null
        };
        return response;
      }
//    } else {
//      print('Tidak Terhubung Dengan Server, Cek jaringan dan data');
//
//      var response = {
//        "status" : 500,
//        "msg": "Error Response : Check Your Network Connection !",
//        "result": null
//      };
//      return response;
//    }
  }

  Future<Map<String, String>> standardHeader() async {
    final all = await _storage.readAll();
    String tokens = all['token'];

    Map<String, String> headersGet = {
      "Content-Type": "application/json",
      "Authorization": "$tokens"
    };

    return headersGet;
  }

  Map<String, String> header() {
    Map<String, String> headersGet = {
      "Accept": "application/json",
    };
    return headersGet;
  }
}
