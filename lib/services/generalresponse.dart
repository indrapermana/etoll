class GeneralResponses {
  final status;
  final msg;

  GeneralResponses(this.status, this.msg);

  GeneralResponses.fromJson(Map<String, dynamic> json)
      : status = json['status'],
        msg = json['msg'];

  Map<String, dynamic> toJson() => {"status": status, "msg": msg};
}
