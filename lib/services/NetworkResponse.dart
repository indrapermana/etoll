import 'dart:convert';

NetworkResponse networkResponseFromJson(String str) => NetworkResponse.fromJson(json.decode(str));

String networkResponseToJson(NetworkResponse data) => json.encode(data.toJson());

class NetworkResponse {
  int status;
  String msg;
  Data result;

  NetworkResponse({
    this.status,
    this.msg,
    this.result,
  });

  factory NetworkResponse.fromJson(Map<String, dynamic> json) => NetworkResponse(
    status: json["status"],
    msg: json["msg"],
    result: Data.fromJson(json["result"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "msg": msg,
    "result": result.toJson(),
  };
}

class Data {
  Data();

  factory Data.fromJson(Map<String, dynamic> json) => Data();

  Map<String, dynamic> toJson() => {};
}
