import 'dart:io';

import 'package:device_info/device_info.dart';

class PlatformDevice {
  static Future<DeviceOS> checkDevice() async {
    String systemName = "";
    String version = "";
    String manufacturer = "";
    String model = "";
    if (Platform.isAndroid) {
      var androidInfo = await DeviceInfoPlugin().androidInfo;
      systemName = androidInfo.version.release;
      version = androidInfo.version.sdkInt.toString();
      manufacturer = androidInfo.manufacturer;
      model = androidInfo.model;
      print('Android $systemName (SDK $version), $manufacturer $model');
      // Android 9 (SDK 28), Xiaomi Redmi Note 7
    }

    if (Platform.isIOS) {
      var iosInfo = await DeviceInfoPlugin().iosInfo;
      systemName = iosInfo.systemName;
      version = iosInfo.systemVersion;
      manufacturer = iosInfo.name;
      model = iosInfo.model;
      print('$systemName $version, $manufacturer $model');
      // iOS 13.1, iPhone 11 Pro Max iPhone
    }

    return DeviceOS(
      systemName: systemName,
      version: version,
      manufacturer: manufacturer,
      model: model
    );
  }
}

class DeviceOS {
  String systemName = "";
  String version = "";
  String manufacturer = "";
  String model = "";

  DeviceOS({
    this.systemName,
    this.version,
    this.manufacturer,
    this.model
  });
}