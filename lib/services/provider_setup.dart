import 'package:etoll/module/login/provider/login_provider.dart';
import 'package:etoll/module/register/provider/resgiter_provider.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

List<SingleChildStatelessWidget> providers = [
  ChangeNotifierProvider<LoginProvider>(create: (context) => LoginProvider()),
  ChangeNotifierProvider<RegisterProvider>(create: (context) => RegisterProvider()),
];

abstract class BaseProvider implements SingleChildStatelessWidget {}
