import 'package:etoll/services/provider_setup.dart';
import 'package:etoll/util/constants.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:etoll/routers/router.dart' as router;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: providers,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: collectionProfile,
        theme: ThemeData(
          primarySwatch: Colors.blue
        ),
        onGenerateRoute: router.generateRoute,
        initialRoute: 'SplashScreen',
      ),
    );
  }
}