import 'package:etoll/util/constants.dart';
import 'package:etoll/util/palette.dart';
import 'package:etoll/widget/StandarText.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';

class GradientButton extends StatelessWidget {
  final ScreenScaler scaler;
  final GestureTapCallback onPressed;
  final String text;

  GradientButton({@required this.scaler, @required this.onPressed, @required this.text});

  @override
  Widget build(BuildContext context) {
    ScreenScaler scaler = ScreenScaler()..init(context);
    return Container(
      margin: EdgeInsets.only(
        top: scaler.getHeight(5),
        bottom: scaler.getHeight(2),
      ),
      width: scaler.getWidth(71),
      height: scaler.getWidth(10),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.centerRight,
          colors: Palette.kitGradients,
        ),
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: onPressed,
          child: Center(
            child: StandarText.label(
              text,
              scaler.getTextSize(fontsz),
            ),
          ),
        ),
      ),
    );
  }
}
