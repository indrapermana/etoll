import 'dart:io';

import 'package:etoll/util/palette.dart';
import 'package:etoll/widget/StandarText.dart';
import 'package:flutter/material.dart';

class ReuseAppBar {
  static getAppBar(String title, BuildContext context, {List<Widget> actions, bool leanding = true, void Function() onPressed}) {
    return AppBar(
      centerTitle: true,
      backgroundColor: Palette.primary1,
      elevation: 0.0,
      automaticallyImplyLeading: false,
      leading: leanding ? IconButton(
        icon: Icon(Platform.isAndroid? Icons.arrow_back : Icons.arrow_back_ios, color: Colors.white,), 
        onPressed: onPressed?? () {
          print("back");
          Navigator.pop(context);
        }
      ) : null,

      title: StandarText.label(title, 19.0),
      flexibleSpace: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerRight,
            end: Alignment.centerLeft,
            colors: <Color>[Palette.primary2, Palette.primary1],
          ),
        ),
      ),
      actions: actions,
    );
  }

  static getAppBarLogo(List<Widget> title, BuildContext context, String image, {List<Widget> actions, bool leanding = true, void Function() onPressed}) {
    return AppBar(
      centerTitle: true,
      backgroundColor: Palette.primary1,
      elevation: 0.0,
      automaticallyImplyLeading: false,
      leading: leanding ? IconButton(
        icon: Image.asset("assets/images/icon/subtraction.png"), 
        onPressed: onPressed?? () => Navigator.pop(context)
      ) : null,

      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 20.0),
            child: Image.asset(image, width: 40,),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: title,
            )
          )
        ],
      ),
      actions: actions,
    );
  }

  static getAppBarColum(List<Widget> title, BuildContext context, {List<Widget> actions, bool leanding = true, void Function() onPressed}) {
    return AppBar(
      centerTitle: true,
      backgroundColor: Colors.transparent,
      elevation: 0.0,
      automaticallyImplyLeading: false,
      leading: leanding ? IconButton(
        icon: Image.asset("assets/images/icon/subtraction.png"), 
        onPressed: onPressed?? () => Navigator.pop(context)
      ) : null,

      title: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: title,
      ),
      flexibleSpace: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerRight,
            end: Alignment.centerLeft,
            colors: <Color>[Palette.primary2, Palette.primary1],
          ),
        ),
      ),
      actions: actions,
    );
  }
}
