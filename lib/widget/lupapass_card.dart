import 'package:etoll/widget/gradient_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';

class LupaPassWidgets extends StatefulWidget {
  @override
  _LupaPassWidgetsState createState() => new _LupaPassWidgetsState();
}

class _LupaPassWidgetsState extends State<LupaPassWidgets> {
  var deviceSize;

  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    deviceSize = MediaQuery.of(context).size;
    ScreenScaler scaler = ScreenScaler()..init(context);
    return Center(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 100.0,
            ),
            lupapassCard(scaler),
          ],
        ),
      ),
    );
  }

  Widget lupapassCard(scaler) {
    return Opacity(
      opacity: 1,
      child: SizedBox(
        height: deviceSize.height / 2 - 20,
        width: deviceSize.width * 0.85,
        child: new Card(
          color: Colors.white,
          elevation: 2.0,
          child: Form(
            child: Padding(
              padding: const EdgeInsets.all(18.0),
              child: SingleChildScrollView(
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new TextField(
                      style: new TextStyle(fontSize: 15.0, color: Colors.black),
                      decoration: new InputDecoration(
                          hintText: "Masukkan Email TDA Passport",
                          labelText: "Email",
                          labelStyle: TextStyle(fontWeight: FontWeight.w700)),
                    ),
                    new SizedBox(
                      height: 10.0,
                    ),
                    new GradientButton(
                      scaler: scaler,
                      text: "Reset Password",
                      onPressed: () => null,
                    ),
                    new SizedBox(
                      height: 10.0,
                    ),
                    new FlatButton(
                      child: Text("Login"),
                      onPressed: () => null,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
