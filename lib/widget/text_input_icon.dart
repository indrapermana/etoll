import 'package:etoll/util/constants.dart';
import 'package:etoll/util/palette.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';

class TextInputIcon extends StatefulWidget {
  final ValueChanged<String> onChanged;
  final IconData iconData;
  final String title;
  final ScreenScaler scaler;

  TextInputIcon({@required this.scaler, @required this.title, this.iconData, @required this.onChanged,});

  @override
  _TextInputIconState createState() => _TextInputIconState();
}

class _TextInputIconState extends State<TextInputIcon> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width*0.67,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10),
              bottomLeft: Radius.circular(10)
            ),
          ),
          padding: widget.scaler.getPaddingLTRB(2, 0, 10, 0),
          child: TextFormField(
            onChanged: (val) => widget.onChanged(val),
            textCapitalization: TextCapitalization.sentences,
            // maxLines: null,
            // keyboardType: TextInputType.multiline,
            cursorColor: Palette.primary3,
            decoration: InputDecoration(
              // labelText: widget.title,
              labelStyle: TextStyle(color: Colors.black54, fontFamily: fonts),
              hintText: widget.title,
              border: InputBorder.none
            ),
          ),
        ),
        Container(
          margin: widget.scaler.getMarginLTRB(62.5, 0, 0, 0),
          // margin: EdgeInsets.only(top: 8.0, left: 15.0),
          width: 40,
          height: 48,
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border(
              // left: BorderSide(
              //   color: Colors.black45,
              //   style: BorderStyle.solid
              // )
            ),
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(10),
              bottomRight: Radius.circular(10)
            )
          ),
          child: Icon(
            widget.iconData??Icons.search,
            color: Palette.primary1,
            size: 20.0,
          ),
        ),
      ],
    );
  }
}