import 'package:etoll/util/palette.dart';
import 'package:etoll/widget/StandarText.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';

bool isDropDownOpened = false;

class CustomDropDown extends StatefulWidget {
  final String label;
  final List<DropDownItem> items;
  final ValueChanged<String> onChanged;
  final String value;
  final IconData iconData;

  const CustomDropDown({Key key, @required this.label, @required this.value, @required this.items, @required this.onChanged, this.iconData}) : super(key: key);
  
  @override
  _CustomDropDownState createState() => _CustomDropDownState();
}

class _CustomDropDownState extends State<CustomDropDown> {

  GlobalKey actionKey;
  double height, width, xPosition, yPosition;
  OverlayEntry floatingDropDown;
  String label = "";

  @override
  void initState() {
    super.initState();
    actionKey = LabeledGlobalKey("DropDown");
    label = widget.label;
  }

  void findDropDownData() {
    RenderBox renderBox = actionKey.currentContext.findRenderObject();
    height = renderBox.size.height;
    width = renderBox.size.width;
    Offset offset = renderBox.localToGlobal(Offset.zero);
    xPosition = offset.dx;
    yPosition = offset.dy;
    print(height);
    print(width);
    print(xPosition);
    print(yPosition);
  }

  OverlayEntry _createFloatingDropdown() {
    return OverlayEntry(builder: (context) {
      return Positioned(
        left: xPosition,
        width: width,
        top: yPosition + height,
        height: widget.items.length * height,
        child: DropDown(
          itemWidth: width,
          itemHeight: height,
          items: widget.items,
          value: widget.value,
          onChanged: (val) {
            var split = val.split("|");
            setState(() {
              label = split[1];
            });
            widget.onChanged(split[0]);
            
          },
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenScaler scaler = ScreenScaler()..init(context);
    return GestureDetector(
      key: actionKey,
      onTap: (){
        setState(() {
          if (isDropDownOpened) {
            floatingDropDown.remove();
          } else {
            findDropDownData();
            floatingDropDown = _createFloatingDropdown();
            Overlay.of(context).insert(floatingDropDown);
          }
          findDropDownData();
          isDropDownOpened = !isDropDownOpened;
        });
      },
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.grey[200]
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Icon(Icons.border_all, color: Palette.primary2,),
            SizedBox(width: 10,),
            Expanded(
              child: StandarText.labelCustom(label, scaler.getTextSize(10)),
            ),
            Icon(Icons.arrow_drop_down)
          ],
        ),
      ),
    );
  }
}

class DropDown extends StatelessWidget {
  final double itemHeight;
  final double itemWidth;
  final List<DropDownItem> items;
  final String value;
  final ValueChanged<String> onChanged;

  const DropDown({Key key, this.itemWidth, this.itemHeight, this.items, this.value, this.onChanged}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      // elevation: 20,
      child: Container(
        // height: items.length * itemHeight,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(8),
            bottomRight: Radius.circular(8)
          ),
          color: Colors.grey[100]
        ),
        child: Column(
          children: items.map<Widget>((item) => InkWell(
            onTap: (){
              isDropDownOpened = false;
              onChanged("${item.value}|${item.text}");
            },
            child: DropDownItem.first(
              text: item.text,
              isSelected: (value==item.value)? true : false,
            ),
          )).toList(),
        ),
      ),
    );
  }
}

class DropDownItem extends StatelessWidget {
  final String value;
  final String text;
  final bool isSelected;
  final bool isFirstItem;
  final bool isLastItem;
  

  const DropDownItem({Key key, this.value, this.text,this.isSelected = false, this.isFirstItem = false, this.isLastItem = false})
      : super(key: key);

  factory DropDownItem.first({String value, String text, IconData iconData, bool isSelected}) {
    return DropDownItem(
      value: value,
      text: text,
      isSelected: isSelected,
      isFirstItem: true,
    );
  }

  factory DropDownItem.last({String value, String text, IconData iconData, bool isSelected}) {
    return DropDownItem(
      value: value,
      text: text,
      isSelected: isSelected,
      isLastItem: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    ScreenScaler scaler = ScreenScaler()..init(context);
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: scaler.getMarginLTRB(5, 0, 5, 0),
      decoration: BoxDecoration(
        // borderRadius: BorderRadius.vertical(
        //   top: isFirstItem ? Radius.circular(8) : Radius.zero,
        //   bottom: isLastItem ? Radius.circular(8) : Radius.zero,
        // ),
        border: Border(
          bottom: BorderSide(
            color: Colors.black45
          )
        ),
        color: isSelected ? Colors.grey[200] : Colors.grey[100],
      ),
      padding: scaler.getPaddingLTRB(0, 0.5, 0, 0.5),
      child: StandarText.labelCustom(text, scaler.getTextSize(10), fontWeight: FontWeight.normal, textAlign: TextAlign.center),
    );
  }
}