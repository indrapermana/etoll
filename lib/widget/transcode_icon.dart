import 'package:etoll/util/palette.dart';
import 'package:etoll/widget/StandarText.dart';
import 'package:flutter/cupertino.dart';

class TransIcon {
  static trans(val, textSize) {
    switch (val) {
      case 1:
        return StandarText.labelfordetail('Top Up', textSize);
        break;
      case 2:
        return StandarText.labelfordetail('Pembayaran', textSize);
        break;
      case 3:
        return StandarText.labelfordetail('Transfer', textSize);
        break;
      case 4:
        return StandarText.labelfordetail('Reversal', textSize);
        break;
      default:
        return StandarText.labelfordetail('Trans', textSize);
        break;
    }
  }

  static transicon(val, scale) {
    switch (val) {
      case 1:
        return ImageIcon(
          AssetImage("assets/images/icon/ico_wallet.png"),
          color: Palette.primary2,
          size: scale,
        );
        break;
      case 2:
        return ImageIcon(
          AssetImage("assets/images/icon/deduct.png"),
          color: Palette.primary2,
          size: scale,
        );
        break;
      case 3:
        return ImageIcon(
          AssetImage("assets/images/icon/ico_transfer.png"),
          color: Palette.primary2,
          size: scale,
        );
        break;
      case 4:
        return ImageIcon(
          AssetImage("assets/images/icon/reversal.png"),
          color: Palette.primary2,
          size: scale,
        );
        break;
      default:
        return StandarText.labelfordetail('Trans', 10);
        break;
    }
  }

  static receiptscr(val, scale) {
    switch (val) {
      case 1:
        return ImageIcon(
          AssetImage("assets/images/icon/ico_wallet.png"),
          color: Palette.primary2,
          size: scale,
        );
        break;
      case 2:
        return Container();
        break;
      case 3:
        return ImageIcon(
          AssetImage("assets/images/icon/ico_transfer.png"),
          color: Palette.primary2,
          size: scale,
        );
        break;
      case 4:
        return ImageIcon(
          AssetImage("assets/images/icon/reversal.png"),
          color: Palette.primary2,
          size: scale,
        );
        break;
      default:
        return StandarText.labelfordetail('Trans', 10);
        break;
    }
  }

  static denomcolor(val) {
    switch (val) {
      case 1:
        return Color(0xff8bc34a);
        break;
      case 2:
        return Color(0xfff44336);
        break;
      default:
        return Color(0xff212121);
        break;
    }
  }
}
