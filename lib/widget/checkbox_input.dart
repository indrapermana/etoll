import 'package:flutter/material.dart';

class CheckboxInput extends StatelessWidget {
  final GestureTapCallback onTap;
  final bool value;

  const CheckboxInput({Key key, this.onTap, this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Icon(value? Icons.check_box : Icons.check_box_outline_blank, color: Colors.white,),
    );
  }
}