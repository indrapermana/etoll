import 'package:etoll/widget/StandarText.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';

class ContectShow extends StatefulWidget {
  final String label;
  final Widget data;

  ContectShow({@required this.label, @required this.data});
  
  @override
  _ContectShowState createState() => _ContectShowState();
}

class _ContectShowState extends State<ContectShow> {

  bool show = false;

  @override
  Widget build(BuildContext context) {
    ScreenScaler scaler = ScreenScaler()..init(context);
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: (){
              setState(() {
                show = !show;
              });
            },
            child: Container(
              padding: scaler.getPaddingLTRB(0.5, 0.5, 0.5, 0.5),
              margin: scaler.getMarginLTRB(0.5, 0.5, 0.5, 0.5),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(color: Colors.black45)
                  )
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  StandarText.label(widget.label, scaler.getTextSize(10)),
                  Icon(show? Icons.keyboard_arrow_down : Icons.keyboard_arrow_right, size: scaler.getTextSize(16),)
                ],
              ),
            ),
          ),

          show? Container(
            padding: scaler.getPaddingLTRB(0.5, 0.5, 0.5, 0.5),
            child: widget.data,
          ) : Container()
        ],
      ),
    );
  }
}
