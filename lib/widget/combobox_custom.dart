import 'package:etoll/widget/StandarText.dart';
import 'package:etoll/widget/combobox_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';

class ComboboxCustom extends StatefulWidget {
  final String label;
  final String hint;
  final String value;
  final Widget iconLeft;
  final Widget iconRight;
  final List<DataItem> data;
  final ValueChanged<String> onChanged;
  final Color color;

  ComboboxCustom({@required this.value, @required this.data, @required this.onChanged, this.hint, this.label, this.iconLeft, this.iconRight, this.color});
  @override
  _ComboboxCustomState createState() => _ComboboxCustomState();
}

class _ComboboxCustomState extends State<ComboboxCustom> {

  bool show = false;
  String value;

  // InputDecoration decoration;
  // var decoration = decoration ?? InputDecoration(focusColor: focusColor),
  // InputDecoration decorationArg = decoration ?? InputDecoration(focusColor: focusColor);
  // InputDecoration effectiveDecoration = decorationArg.applyDefaults(
  //   Theme.of(field.context).inputDecorationTheme,
  // );

  @override
  void initState() {
    super.initState();

    value = widget.value;
    
    
  }

  // void cek(){
  //   for (var i = 0; i < widget.data.length; i++) {
      
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    ScreenScaler scaler = ScreenScaler()..init(context);
    return Stack(
      children: [
        InkWell(
          onTap: (){
            if(widget.data.length>0){
              setState(() {
                show = !show;
              });
            }
          },
          child: Container(
            margin: scaler.getMarginLTRB(3, 1, 3, 0),
            child: Column(
              children: [
                // StandarText.labelCustom("judul", 12.0),
                Container(
                  // width: width,
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: widget.color?? Colors.white,
                    borderRadius: show? BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10)
                    ) : BorderRadius.circular(10)
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      widget.iconLeft??Container(),
                      SizedBox(width: 10,),
                      Expanded(
                        child: value!=null? StandarText.labelCustom(value, scaler.getTextSize(11), fontWeight: FontWeight.normal) : StandarText.labelCustom(widget.hint, scaler.getTextSize(11), color: Colors.black54, fontWeight: FontWeight.normal)
                      ),
                      widget.iconRight??Icon(Icons.arrow_drop_down_outlined, color: widget.data.length>0? Colors.black : Colors.black45,)
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        show? Container(
              margin: scaler.getMarginLTRB(3, 6, 3, 0),
              decoration: BoxDecoration(
                color: Colors.grey[300],
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10)
                )
              ),
              child: Column(
                children: widget.data.map((data) => InkWell(
                  onTap: (){
                    setState(() {
                      value = data.name;
                      show = false;  
                      widget.onChanged(data.id);
                    });
                  },
                  child: Container(
                    width: width,
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                      border: Border(
                        top: BorderSide(color: Colors.black26)
                      )
                    ),
                    child: StandarText.labelCustom(data.name, 12.0, fontWeight: FontWeight.normal),
                  ),
                )).toList(),
              ),
            
        ) : Container()
      ],
    );
  }
}