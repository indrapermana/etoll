import 'package:etoll/util/constants.dart';
import 'package:etoll/util/palette.dart';
import 'package:etoll/widget/StandarText.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';

class BorderButton extends StatelessWidget {

  final ScreenScaler scaler;
  final GestureTapCallback onPressed;
  final String text;
  final double fontsSize;
  final Color colorText;
  final Color colorButton;
  final Color colorBorder;
  final double width;
  final double height;
  final EdgeInsetsGeometry margin;
  final BorderRadiusGeometry borderRadius;

  BorderButton({@required this.scaler, @required this.text, @required this.onPressed, this.fontsSize, this.colorText, this.colorButton, this.colorBorder, this.width, this.height, this.margin, this.borderRadius});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin?? EdgeInsets.only(
        bottom: scaler.getHeight(2),
      ),
      width: width?? scaler.getWidth(71),
      height: height?? scaler.getWidth(10),
      child: RaisedButton(
        onPressed: onPressed,
        child: StandarText.label(text, scaler.getTextSize(fontsSize??fontsz), colorText?? Palette.primary1),
        shape: RoundedRectangleBorder(
          borderRadius: borderRadius?? BorderRadius.circular(10.0),
          side: BorderSide(color: colorBorder?? Palette.primary2),
        ),
        color: colorButton?? Colors.white,
      ),
    );
  }
}