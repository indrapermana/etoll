import 'dart:io';

import 'package:etoll/util/palette.dart';
import 'package:etoll/widget/StandarText.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';

class ScaffoldCustom extends StatelessWidget {

  final String title;
  final String image;
  final Widget child;
  final Widget childStack;
  final bool landscape;
  final bool isLeading;
  final VoidCallback onPressed;
  final Widget iconLeft;
  final Widget iconRight;
  final Widget bottomSheet;
  final Widget drawer;
  final double width;
  final double height;

  const ScaffoldCustom({Key key, this.title, @required this.image, @required this.child, this.childStack, this.landscape = false, this.isLeading = false, this.onPressed, this.iconLeft, this.iconRight, this.bottomSheet, this.drawer, this.width, this.height}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ScreenScaler scaler = ScreenScaler()..init(context);
    return Scaffold(
      key: key,
      drawer: drawer,
      backgroundColor: Palette.primary1,
      body: Container(
        width: scaler.getWidth(100),
        height: scaler.getHeight(100),
        margin: landscape? scaler.getMarginLTRB(0, 8, 0, 0) : scaler.getMarginLTRB(0, 5, 0, 0),
        padding: scaler.getPaddingLTRB(1, 1, 1, 0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20)
          ),
        ),
        child: Stack(
          children: [
            Container(
              width: scaler.getWidth(100),
              height: landscape? scaler.getWidth(6) :  scaler.getHeight(6),
              margin: scaler.getMarginLTRB(0, 0, 0, 3),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  isLeading? IconButton(
                      icon: Icon(Platform.isAndroid? Icons.arrow_back : Icons.arrow_back_ios, size: landscape? scaler.getTextSize(14) : scaler.getTextSize(13),),
                      onPressed: onPressed?? (){
                        Navigator.pop(context);
                      }
                  ) : iconLeft?? Container(),
                  Expanded(
                    child: Container(
                      // color: Colors.yellow,
                      margin: iconRight==null? scaler.getMarginLTRB(0, 0, 6, 0) : null,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        // crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          // Icon(icon??Icons.book, color: Colors.white,),
                          image!=null? Image.asset(
                            // image??"assets/images/logo/icon_telkom.png",
                            image,
                            width: landscape? scaler.getWidth(70) : iconRight!=null? scaler.getWidth(60) : width?? scaler.getWidth(70), 
                            height: landscape? scaler.getWidth(5) : iconRight!=null? scaler.getWidth(10) : height?? scaler.getWidth(10),
                          ) : Container(),
                          title!=null && image!=null? SizedBox(width: 5,) : Container(),
                          title!=null? StandarText.labelCustom(title, scaler.getTextSize(14)) : Container()
                        ],
                      ),
                    ),
                  ),
                  iconRight??Container()
                ],
              ),
            ),

            Container(
              margin: landscape? scaler.getMarginLTRB(0, 13, 0, 0) : scaler.getMarginLTRB(0, 7, 0, 0),
              child: child,
            ),

            childStack??Container()
          ],
        ),
      ),
      bottomSheet: bottomSheet,
    );
  }
}