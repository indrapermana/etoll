import 'package:etoll/util/palette.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';

class BorderIconButton extends StatelessWidget {

  final ScreenScaler scaler;
  final GestureTapCallback onPressed;
  final IconData icon;

  BorderIconButton({@required this.scaler, @required this.onPressed, @required this.icon});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        bottom: scaler.getHeight(2),
      ),
      width: scaler.getWidth(71),
      height: scaler.getWidth(10),
      child: RaisedButton(
        onPressed: onPressed,
        child: Icon(icon, color: Palette.primary2, size: scaler.getTextSize(18),),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
          side: BorderSide(color: Palette.primary2),
        ),
        color: Colors.white,
      ),
    );
  }
}