import 'package:etoll/util/palette.dart';
import 'package:etoll/widget/row_label.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';

class Expansion extends StatefulWidget {
  final ScreenScaler scaler;
  final String title;
  final String dataTitle;
  final List<Widget> list;
  final String url;

  Expansion(this.scaler, {this.title, this.dataTitle, this.list, this.url});
  
  @override
  _ExpansionState createState() => _ExpansionState();
}

class _ExpansionState extends State<Expansion> {

  bool show = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
            onTap: () {
              setState(() {
                show = !show;
              });
            },
            child: Container(
              width: widget.scaler.getWidth(100),
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              decoration: BoxDecoration(
                color: Palette.primary1,
                borderRadius: BorderRadius.all(Radius.circular(15)),
                boxShadow: [
                  BoxShadow(
                    color: Palette.shadowbox,
                    blurRadius: 5.0, // soften the shadow
                    spreadRadius: 2.0, //extend the shadow
                    offset: Offset(
                      0.2, // Move to right 10  horizontally
                      1.0, // Move to bottom 5 Vertically
                    ),
                  )
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: RowLabel(scaler: widget.scaler, label: widget.title, value: widget.dataTitle, color: Colors.white,)
                  ),
                  show? Icon(Icons.arrow_drop_up, color: Colors.white,) : Icon(Icons.arrow_drop_down, color: Colors.white,)
                ],
              ),
            ),
          ),
          show? Container(
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
              color: Palette.bgcolor,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(15),
                bottomRight: Radius.circular(15)
              ),
              // borderRadius: BorderRadius.all(Radius.circular(15)),
              boxShadow: [
                BoxShadow(
                  color: Palette.shadowbox,
                  blurRadius: 5.0, // soften the shadow
                  spreadRadius: 2.0, //extend the shadow
                  offset: Offset(
                    0.2, // Move to right 10  horizontally
                    1.0, // Move to bottom 5 Vertically
                  ),
                )
              ],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: widget.list,
            ),
          ) : Container(),
        ],
      ),
    );
  }
}