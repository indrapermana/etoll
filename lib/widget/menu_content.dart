import 'package:etoll/util/palette.dart';
import 'package:etoll/widget/StandarText.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';

class MenuContent {
  static menu(List<Widget> data){
    return Container(
      margin: EdgeInsets.only(bottom: 0.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: data
      ),
    );
  }
  
  static menuHorizontal(ScreenScaler scaler, BuildContext context, String image, String label, void Function() onTap, {bool imgAssets = true, double textSize}){
    image = (image=="")? 'assets/images/logo/logo_telkom.png' : image;
    return InkWell(
        onTap: onTap,
        child: Container(
          // height: scaler.getHeight(10),
          // width: scaler.getWidth(21),
          child: Container(
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
              color: Palette.primary5,
              // border: Border.all(color: Colors.black26, width: 3),
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Palette.shadowbox,
                  blurRadius: 2.0, // soften the shadow
                  spreadRadius: 1.0, //extend the shadow
                  offset: Offset(
                    0.0, // Move to right 10  horizontally
                    1.0, // Move to bottom 5 Vertically
                  ),
                )
              ],
            ),
            child: Column(
              children: [
                imgAssets? Image.asset(image, width: scaler.getWidth(10), height: scaler.getWidth(8),) : Image.network(image, width: scaler.getWidth(20), height: scaler.getWidth(20),),
                StandarText.labelCustom(
                  label, 
                  textSize?? scaler.getTextSize(8.5),
                  color: Palette.primary1,
                  fontWeight: FontWeight.normal,
                  textAlign: TextAlign.center
                )
              ],
            ),
          ),
        ),
    );
  }

  static menuHorizontalSmall(ScreenScaler scaler, BuildContext context, String image, String label, void Function() onTap, [bool imgAssets = true]){
    image = (image=="")? 'assets/images/logo/logo_telkom.png' : image;
    return Container(
      height: scaler.getHeight(12),
      width: scaler.getWidth(17),
      child: InkWell(
        onTap: onTap,
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                boxShadow: [
                  BoxShadow(
                    color: Palette.shadowbox,
                    blurRadius: 5.0, // soften the shadow
                    spreadRadius: 2.0, //extend the shadow
                    offset: Offset(
                      0.2, // Move to right 10  horizontally
                      1.0, // Move to bottom 5 Vertically
                    ),
                  )
                ],
              ),
              child: imgAssets? Image.asset(image, width: scaler.getWidth(10), height: scaler.getWidth(10),) : Image.network(image, width: scaler.getWidth(20), height: scaler.getWidth(20),),
            ),
            SizedBox(height: 10,),
            StandarText.labelCenter(
              label,
              scaler.getTextSize(9),
            ),
          ],
        ),
      ),
    );
  }

  static menuVertical(ScreenScaler scaler, BuildContext context, String image, String label, void Function() onTap){
    image = (image=="")? 'assets/images/logo/logo_telkom.png' : image;
    return Container(
      padding: EdgeInsets.all(scaler.getWidth(1)),
      margin: EdgeInsets.only(
        bottom: scaler.getHeight(5),
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Palette.shadowbox,
            blurRadius: 5.0, // soften the shadow
            spreadRadius: 2.0, //extend the shadow
            offset: Offset(
              0.2, // Move to right 10  horizontally
              1.0, // Move to bottom 5 Vertically
            ),
          )
        ],
      ),
      child: InkWell(
        onTap: onTap,
        child: Row(
          children: <Widget>[
            Image.asset(image, width: scaler.getWidth(20), height: scaler.getWidth(20),),
            SizedBox(width: 20,),
            Expanded(
              child: StandarText.label(
                label,
                16.0,
              ),
            )
          ],
        ),
      )
    );
  }

  static menuContentFoto({@required ScreenScaler scaler, @required BuildContext context, @required Image image, @required String label, @required void Function() onTap, @required bool validation}){
    return Container(
      height: scaler.getHeight(19),
      width: scaler.getWidth(20),
      child: InkWell(
        onTap: onTap,
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Palette.shadowbox,
                    blurRadius: 5.0, // soften the shadow
                    spreadRadius: 2.0, //extend the shadow
                    offset: Offset(
                      0.2, // Move to right 10  horizontally
                      1.0, // Move to bottom 5 Vertically
                    ),
                  )
                ],
                border: (validation)? Border.all(color: Palette.primary2, width: 2) : null 
              ),
              child: image,
            ),
            SizedBox(height: scaler.getWidth(1)),
            StandarText.labelCenter(
              label,
              scaler.getTextSize(10.0),
              (validation)? Palette.primary2 : null 
            )
          ],
        ),
      ),
    );
  }
}