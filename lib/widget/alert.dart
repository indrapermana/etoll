import 'package:etoll/util/palette.dart';
import 'package:etoll/widget/StandarText.dart';
import 'package:etoll/widget/button_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';

void alertDialog({@required BuildContext context, @required ScreenScaler scaler, Widget content, String title, String msg, List<Widget> actions, Color textColor, double height}) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      //if(Platform.isAndroid){
        return AlertDialog(
          // title: title!=null ? StandarText.labelCustom(title, scaler.getTextSize(11), textAlign: TextAlign.center) : null,
          contentPadding: scaler.getPaddingLTRB(2, 1.1, 2, 1.1),
          content: content?? Container(
            height: height?? scaler.getHeight(25),
            width: scaler.getWidth(100),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                title!=null? StandarText.labelCustom(title, scaler.getTextSize(11), textAlign: TextAlign.center) : Container(),
                title!=null? SizedBox(height: 10,) : Container(),
                StandarText.labelCustom(
                  msg, scaler.getTextSize(11),
                  fontWeight: FontWeight.normal, color: textColor??Colors.black,
                  textAlign: TextAlign.center
                ),
                Container(
                  width: scaler.getWidth(100),
                  margin: scaler.getMarginLTRB(0, 2, 0, 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: actions??[
                      ButtonColor(
                        width: scaler.getWidth(21),
                        height: scaler.getWidth(7),
                        scaler: scaler,
                        label: "Oke",
                        color: Palette.primary1, 
                        lableColor: Colors.white,
                        onPressed: () => Navigator.pop(context)
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          // actions: actions ?? <Widget>[
            // ButtonColor(
            //   width: scaler.getWidth(21),
            //   height: scaler.getWidth(7),
            //   scaler: scaler,
            //   label: "Oke", 
            //   color: Palette.primary1, 
            //   lableColor: Colors.white,
            //   onPressed: () => Navigator.pop(context)
            // )
          // ],
        );
      /*} else {
        return CupertinoAlertDialog(
          title: title!=null? Text(title, style: TextStyle(color: color??Colors.red, fontSize: scaler.getTextSize(12)), textAlign: TextAlign.center,) : null,
          content: body,
          actions: actions?? <Widget>[
            ButtonColor(
              width: scaler.getWidth(10),
              height: scaler.getWidth(7),
              scaler: scaler, 
              label: "Oke", 
              color: Palette.primary1, 
              lableColor: Colors.white,
              onPressed: () => Navigator.pop(context)
            )
          ],
        );
      }*/

    },
  );
}