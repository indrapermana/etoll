import 'package:flutter/material.dart';

class Loadingwidget extends StatelessWidget {
  const Loadingwidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        padding: EdgeInsets.all(15),
        child: CircularProgressIndicator(
            strokeWidth: 3, valueColor: AlwaysStoppedAnimation<Color>(Colors.redAccent)),
      ),
    );
  }
}
