import 'package:etoll/widget/StandarText.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';

class RowLabel extends StatelessWidget {
  final ScreenScaler scaler;
  final String label;
  final String value;
  final Color color;
  final FontWeight fontWeight;
  final TextAlign textAlign;
  final double fontSize1;
  final double fontSize2;
  final IconData icon;
  final bool hr;
  final EdgeInsetsGeometry margin;
  final GestureTapCallback onTap;

  RowLabel({@required this.scaler, @required this.label, @required this.value, this.color, this.fontWeight, this.textAlign, this.fontSize1, this.fontSize2, this.icon, this.hr = false, this.margin, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin?? EdgeInsets.only(bottom: 15),
      child: InkWell(
        onTap: onTap,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: scaler.getWidth(1)),
              child: StandarText.subtitle(
                label,
                scaler.getTextSize(fontSize1??10),
                color
              )
            ),
            Container(
              margin: EdgeInsets.only(bottom: scaler.getWidth(1)),
              padding: hr? EdgeInsets.only(bottom: 5) : null,
              decoration: hr? BoxDecoration(
                border: Border(
                  bottom: BorderSide(color: Colors.black45)
                )
              ) : null,
              child: (icon==null)? StandarText.labelCustom(
                value,
                scaler.getTextSize(fontSize2??11),
                color: color,
                fontWeight: fontWeight,
                textAlign: textAlign
              ) : Row(
                children: <Widget>[
                  Expanded(
                    child: StandarText.label(
                      value,
                      scaler.getTextSize(fontSize2??11),
                      color
                    )
                  ),
                  Icon(icon, color: Colors.green,)
                ],
              )
            ),
          ],
        ),
      ),
    );
  }
}