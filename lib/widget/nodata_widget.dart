import 'package:etoll/widget/StandarText.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';

class NoDataWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenScaler scaler = ScreenScaler()..init(context);
    return Center(
      child: Container(
        padding: EdgeInsets.all(5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: scaler.getHeight(45),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/images/banner/nodata.png'), fit: BoxFit.contain),
              ),
            ),
            StandarText.label('Tidak ada data', 10.0)
          ],
        ),
      ),
    );
  }
}
