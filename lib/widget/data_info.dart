import 'package:etoll/util/constants.dart';
import 'package:etoll/widget/StandarText.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';

class DataInfo extends StatelessWidget {
  final ScreenScaler scaler;
  final String title;
  final String value1;
  final String value2;

  DataInfo({@required this.scaler, @required this.title, @required this.value1, @required this.value2});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: scaler.getMarginLTRB(0, 0, 0, 2),
      padding: scaler.getPaddingLTRB(0, 0, 0, 1),
      width: scaler.getWidth(100),
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(color: Colors.black38))
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(bottom: scaler.getWidth(1)),
            child: StandarText.subtitle(
              title,
              scaler.getTextSize(11),
            )
          ),
          Container(
            margin: EdgeInsets.only(bottom: scaler.getWidth(1), left: scaler.getWidth(3)),
            child: StandarText.label(
              value1,
              scaler.getTextSize(fontsz),
            )
          ),
          Container(
            margin: EdgeInsets.only(bottom: scaler.getWidth(1), left: scaler.getWidth(3)),
            child: StandarText.label(
              value2,
              scaler.getTextSize(fontsz),
            )
          ),
        ],
      )
    );
  }
}