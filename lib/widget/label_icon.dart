import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';
import 'package:etoll/widget/StandarText.dart';

class LabelIcon extends StatelessWidget {
  final ScreenScaler scaler;
  final String label;
  final Color color;
  final Color iconColor;
  final FontWeight fontWeight;
  final TextAlign textAlign;
  final double fontSize;
  final bool isIcon;
  final IconData icon;
  final String image;
  final GestureTapCallback onTap;

  LabelIcon({@required this.scaler, @required this.label, this.color, this.iconColor, this.fontWeight, this.textAlign, this.fontSize, this.isIcon = true, this.icon, this.image, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: InkWell(
        onTap: onTap,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            StandarText.labelCustom(label, fontSize, color: color, fontWeight: fontWeight, textAlign: textAlign),
            SizedBox(width: 10,),
            isIcon? Icon(icon, color: iconColor,) : Image.asset(image)
          ],
        ),
      ),
    );
  }
}