import 'package:etoll/util/constants.dart';
import 'package:etoll/util/palette.dart';
import 'package:flutter/material.dart';

class StandarText {

  static label(judul, fontsize, [Color color]) {
    return Text(
      '$judul',
      textAlign: TextAlign.left,
      softWrap: true,
      style: TextStyle(
          fontSize: fontsize,
          fontFamily: fonts,
          fontWeight: FontWeight.bold,
          color: color??Palette.primary3),
    );
  }

  static labelCustom(judul, fontsize, {Color color, FontWeight fontWeight, TextAlign textAlign, FontStyle fontStyle}) {
    return Text(
      '$judul',
      textAlign: textAlign??TextAlign.left,
      softWrap: true,
      style: TextStyle(
          fontSize: fontsize,
          fontFamily: fonts,
          fontWeight: fontWeight??FontWeight.bold,
          color: color??Palette.primary3,
        fontStyle: fontStyle??FontStyle.normal
      ),
    );
  }

  static labelRight(judul, fontsize, [Color color]) {
    return Text(
      '$judul',
      textAlign: TextAlign.right,
      softWrap: true,
      style: TextStyle(
          fontSize: fontsize,
          fontFamily: fonts,
          fontWeight: FontWeight.bold,
          color: color??Palette.primary3),
    );
  }

  static labelCenter(judul, fontsize, [Color color]) {
    return Text(
      '$judul',
      textAlign: TextAlign.center,
      softWrap: true,
      style: TextStyle(
        fontSize: fontsize,
        fontFamily: fonts,
        fontWeight: FontWeight.bold,
        color: color??Palette.primary3
      ),
    );
  }

  static labelputih(judul, fontsize) {
    return Text(
      '$judul',
      textAlign: TextAlign.left,
      softWrap: true,
      style: TextStyle(
          fontSize: fontsize,
          fontFamily: fonts,
          fontWeight: FontWeight.bold,
          color: Colors.white),
    );
  }

  static redlabel(judul, fontsize) {
    return Text(
      '$judul',
      textAlign: TextAlign.center,
      softWrap: true,
      style: TextStyle(
          fontSize: fontsize,
          fontFamily: fonts,
          fontWeight: FontWeight.bold,
          color: Colors.red),
    );
  }

  static greenlabel(judul, fontsize) {
    return Text(
      '$judul',
      textAlign: TextAlign.center,
      softWrap: true,
      style: TextStyle(
          fontSize: fontsize,
          fontFamily: fonts,
          fontWeight: FontWeight.bold,
          color: Colors.green),
    );
  }

  static subtitle(judul, fontsize, [Color color]) {
    return Text(
      '$judul',
      textAlign: TextAlign.center,
      softWrap: true,
      style: TextStyle(
        fontSize: fontsize,
        fontFamily: fonts,
//          fontWeight: FontWeight.bold,
        color: color?? Colors.grey,
      ),
    );
  }

  static Widget labelBottomNav(String namaBottomNav, double textSize) {
    return Text(
      '$namaBottomNav',
      textAlign: TextAlign.center,
      softWrap: true,
      style: TextStyle(
        fontSize: textSize,
        fontFamily: fonts,
        fontWeight: FontWeight.bold,
        color: Palette.primary2,
        fontStyle: FontStyle.normal,
      ),
    );
  }

  static denomtext(String nominal, double textSize) {
    return Text(
      '$nominal',
      textAlign: TextAlign.center,
      softWrap: true,
      style: TextStyle(
        fontSize: textSize,
        fontFamily: fonts,
        fontWeight: FontWeight.bold,
        color: Palette.denomNoActive,
        fontStyle: FontStyle.normal,
      ),
    );
  }

  static labelfordetail(String s, double textSize) {
    return Text(
      '$s',
      textAlign: TextAlign.left,
      softWrap: true,
      style: TextStyle(
        fontSize: textSize,
        fontFamily: fonts,
        color: Palette.primary3,
        fontStyle: FontStyle.normal,
      ),
    );
  }

  static labelgrey(String s, double textSize) {
    return Text(
      '$s',
      textAlign: TextAlign.left,
      softWrap: true,
      style: TextStyle(
        fontSize: textSize,
        fontFamily: fonts,
        color: Palette.labelgrey,
        fontStyle: FontStyle.normal,
      ),
    );
  }

  static headermerah(String s, double textSize) {
    return Text(
      '$s',
      textAlign: TextAlign.left,
      softWrap: true,
      style: TextStyle(
          fontSize: textSize,
          fontFamily: fonts,
          color: Palette.primary2,
          fontStyle: FontStyle.normal,
          fontWeight: FontWeight.bold),
    );
  }

  static suksesmessage(String strmessage, double textSize) {
    return Text(
      '$strmessage',
      textAlign: TextAlign.center,
      softWrap: true,
      style: TextStyle(
          fontSize: textSize,
          fontFamily: fonts,
          color: Palette.primary1,
          fontStyle: FontStyle.normal,
          fontWeight: FontWeight.bold),
    );
  }

  static labeltopup(String strmessage, double textSize) {
    return Text(
      '$strmessage',
      textAlign: TextAlign.center,
      softWrap: true,
      style: TextStyle(
          fontSize: textSize,
          fontFamily: fonts,
          color: Colors.lightGreenAccent,
          fontStyle: FontStyle.normal,
          fontWeight: FontWeight.normal),
    );
  }

  static headerCenter(judul, fontsize) {
    return Text(
      '$judul',
      style: TextStyle(
        fontSize: fontsize,
        fontFamily: fonts,
        fontWeight: FontWeight.bold,
        color: Colors.white,
      ),
      textAlign: TextAlign.center,
      softWrap: true,
    );
  }
}
