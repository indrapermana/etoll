import 'package:etoll/module/helper/view/helper_screen.dart';
import 'package:etoll/module/home/view/detail_news_screen.dart';
import 'package:etoll/module/home/view/promosi_screen.dart';
import 'package:etoll/module/login/view/login_screen.dart';
import 'package:etoll/module/register/view/resgiter_screen.dart';
import 'package:etoll/module/setting/view/settting_screen.dart';
import 'package:etoll/module/splash/view/splash_screen.dart';
import 'package:etoll/module/transaksi/view/transaksi_screen.dart';
import 'package:flutter/material.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  
  switch (settings.name) {
    case 'LoginScreen':
      return MaterialPageRoute(
        settings: routeSettings(settings), 
        builder: (context) => LoginScreen());
      break;
    case 'SplashScreen':
      return MaterialPageRoute(
        settings: routeSettings(settings), 
        builder: (context) => SplashScreen());
      break;
    case 'RegisterScreen':
      return MaterialPageRoute(
        settings: routeSettings(settings), 
        builder: (context) => RegisterScreen());
      break;
    case 'HelperScreen':
      return MaterialPageRoute(
        settings: routeSettings(settings), 
        builder: (context) => HelperScreen());
      break;
    case 'TransaksiScreen':
      return MaterialPageRoute(
        settings: routeSettings(settings), 
        builder: (context) => TransaksiScreen());
      break;
    case 'SettingScreen':
      return MaterialPageRoute(
        settings: routeSettings(settings), 
        builder: (context) => SettingScreen());
      break;
    case 'DetailNewsScreen':
      return MaterialPageRoute(
        settings: routeSettings(settings), 
        builder: (context) => DetailNewsScreen());
      break;
    case 'PromosiScreen':
      return MaterialPageRoute(
        settings: routeSettings(settings), 
        builder: (context) => PromosiScreen());
      break;
    default:
      return MaterialPageRoute(
        settings: RouteSettings(name: 'SplashScreen'),
        builder: (context) => SplashScreen()
      );
      break;
  }
}

RouteSettings routeSettings(RouteSettings settings) =>
    RouteSettings(name: settings.name);