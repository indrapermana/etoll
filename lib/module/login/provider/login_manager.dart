import 'package:etoll/module/login/model/response_login_model.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

final _storage = FlutterSecureStorage();

class LoginManager {
  static savetostorage(DataLogin login) async {
    _addToStorage("loggedIn", "true");
    _addToStorage("kode", login.kode);
    _addToStorage("nama", login.nama);
    _addToStorage("username", login.username);
    _addToStorage("email", login.emailAddress);
    _addToStorage("token", login.token);
    _addToStorage("profile_pic", login.profilePic);
  }

  static _addToStorage(String key, val) {
    _storage.write(
      key: key,
      value: val.toString(),
    );
  }

  static getStorage(String key) async {
    return await _storage.read(key: key);
  }

  static clearStorage() async {
    await _storage.deleteAll();
  }
}
