class ResponseLoginModel {
  ResponseLoginModel({
    this.status,
    this.msg,
    this.result,
  });

  int status;
  String msg;
  DataLogin result;

  factory ResponseLoginModel.fromJson(Map<String, dynamic> json) => ResponseLoginModel(
    status: json["status"],
    msg: json["msg"],
    result: json["result"]==null? DataLogin() : DataLogin.fromJson(json["result"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "msg": msg,
    "result": result==null? null : result.toJson(),
  };
}

class DataLogin {
  DataLogin({
    this.token = "",
    this.kode = "",
    this.username = "",
    this.emailAddress = "",
    this.profilePic = "",
    this.nama = "",
  });

  String token;
  String kode;
  String username;
  String emailAddress;
  String profilePic;
  String nama;

  factory DataLogin.fromJson(Map<String, dynamic> json) => DataLogin(
    token: json["token"],
    kode: json["kode"],
    username: json["username"],
    emailAddress: json["email_address"],
    profilePic: json["profile_pic"],
    nama: json["nama"],
  );

  Map<String, dynamic> toJson() => {
    "token": token,
    "kode": kode,
    "username": username,
    "email_address": emailAddress,
    "profile_pic": profilePic,
    "nama": nama,
  };
}