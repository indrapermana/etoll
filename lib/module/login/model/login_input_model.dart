class LoginInputModel {
  String username;
  String password;

  LoginInputModel({this.username, this.password});

  factory LoginInputModel.fromJson(Map<String, dynamic> json) => LoginInputModel(
    username: json["username"],
    password: json["password"],
  );

  Map<String, dynamic> toJson() => {
    "username": username,
    "password": password,
  };
}
