import 'package:etoll/module/home/view/home_screen.dart';
import 'package:etoll/module/login/provider/login_provider.dart';
import 'package:etoll/util/constants.dart';
import 'package:etoll/util/palette.dart';
import 'package:etoll/widget/StandarText.dart';
import 'package:etoll/widget/button_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    LoginProvider lginAuth = Provider.of<LoginProvider>(context);

    if (lginAuth.validLogin) {
      return HomeScreen();
    } else {
      return LoginStandart();
    }
  }
}

class LoginStandart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenScaler scaler = ScreenScaler()..init(context);
    GlobalKey<FormState> _key = GlobalKey<FormState>();
    return Scaffold(
      backgroundColor: Palette.bgcolor,
      body: SingleChildScrollView(
        padding: scaler.getPaddingLTRB(5, 10, 5, 2),
        child: Form(
          key: _key,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(child: Image.asset("assets/images/logo/logo.png")),
              SizedBox(height: 50),
              StandarText.labelCustom("Login ke akun anda", scaler.getTextSize(11), fontWeight: FontWeight.w600),
              
              Consumer<LoginProvider>(
                builder: (context, load, _) =>Container(
                  margin: scaler.getMarginLTRB(0, 1, 1, 0),
                  child: TextFormField(
                    onChanged: (val) => load.username = val,
                    // validator: (value) {
                    //   int len = value.length;
                    //   if (len == 0) {
                    //     return "Username Can not be empty";
                    //   }
                    //   return null;
                    // },
                    style: TextStyle(color: Colors.black45),
                    decoration: InputDecoration(
                      hintText: '',
                      labelText: 'Email',
                      labelStyle: TextStyle(
                          color: Colors.black45,
                          fontFamily: fonts,
                          fontWeight: FontWeight.bold),
                      prefixIcon: Icon(
                        Icons.person_outline,
                        color: Palette.primary1,
                      ),
                      fillColor: Colors.black45,
                      errorStyle: TextStyle(color: Palette.primary2),
                      contentPadding: EdgeInsets.all(0),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: BorderSide(
                          color: Palette.primary1,
                        ),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: BorderSide(
                          color: Palette.bordercolor,
                          width: 2.0,
                        ),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: BorderSide(
                          color: Palette.primary2,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Consumer<LoginProvider>(
                builder: (context, load, _) => Container(
                  margin: scaler.getMarginLTRB(0, 2, 1, 0),
                  child: TextFormField(
                    // validator: (value) {
                    //   int len = value.length;
                    //   if (len == 0) {
                    //     return "Password Can not be empty";
                    //   }
                    //   return null;
                    // },
                    onChanged: (val) => load.password = val,
                    obscureText: load.hidePassword,
                    style: TextStyle(color: Colors.black45),
                    decoration: InputDecoration(
                      labelText: 'Kata Sandi',
                      labelStyle: TextStyle(
                          color: Colors.black45,
                          fontFamily: fonts,
                          fontWeight: FontWeight.bold),
                      hintText: '',
                      prefixIcon: Icon(
                        Icons.lock_outline,
                        color: Palette.primary1,
                      ),
                      errorStyle:
                          TextStyle(color: Palette.primary2),
                      contentPadding: EdgeInsets.all(0),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: BorderSide(
                          color: Palette.primary1,
                        ),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: BorderSide(
                          color: Palette.bordercolor,
                          width: 2.0,
                        ),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: BorderSide(
                          color: Palette.primary2,
                        ),
                      ),
                      suffixIcon: GestureDetector(
                        onTap: () =>
                            load.hidePassword = !load.hidePassword,
                        child: Icon(
                          load.hidePassword
                              ? Icons.visibility_off
                              : Icons.visibility,
                          color: Palette.primary1,
                        ),
                      ),
                      isDense: true
                    ),
                  ),
                ),
              ),

              Consumer<LoginProvider>(
                builder: (context, load, _) => (load.message !="")
                  ? Center(
                    child: Container(
                        margin: EdgeInsets.only(
                          top: scaler.getWidth(3),
                        ),
                        child: StandarText.redlabel(
                            load.message, scaler.getTextSize(10), ),
                      ),
                  )
                  : Container()
              ),
              Consumer<LoginProvider>(
                builder: (context, load, _) => Center(
                  child: Container(
                    margin: EdgeInsets.only(
                      top: scaler.getHeight(5),
                      bottom: scaler.getHeight(5)
                    ),
                    // width: scaler.getWidth(25),
                    height: scaler.getWidth(10),
                    decoration: BoxDecoration(
                      color: Palette.primary1,
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () {
                          if (_key.currentState.validate()) {
                            _key.currentState.save();
                            load.isLoading = true;
                            load.message = '';

                            load.submitLoginFlash();
                            // load.submitLogin();
                          }
                        },
                        child: Center(
                          child: StandarText.label(
                            "Masuk",
                            scaler.getTextSize(fontsz),
                            Colors.white),
                        ),
                      ),
                    ),
                  ),
                )
              ),

              Center(
                child: StandarText.labelCustom("Atau masuk dengan", scaler.getTextSize(10), fontWeight: FontWeight.normal),
              ),

              Container(
                width: scaler.getWidth(100),
                margin: scaler.getMarginLTRB(0, 2, 0, 2),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    icon(scaler, "assets/images/icon/Icon-logo-google.png"),
                    icon(scaler, "assets/images/icon/Icon-logo-facebook.png"),
                    icon(scaler, "assets/images/icon/Icon-logo-twitter.png"),
                  ],
                ),
              ),

              Container(
                width: scaler.getWidth(100),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    StandarText.labelCustom("Tidak memiliki akun?", scaler.getTextSize(9), fontWeight: FontWeight.normal),
                    SizedBox(width: 8,),
                    ButtonColor(
                      scaler: scaler, 
                      label: "Daftar",
                      lableColor: Colors.white,
                      labelSize: scaler.getTextSize(9),
                      color: Palette.primary2, 
                      width: scaler.getWidth(10),
                      height: scaler.getWidth(4),
                      padding: scaler.getPaddingLTRB(0, 0, 0, 0),
                      borderRadius: BorderRadius.circular(5.0),
                      margin: scaler.getMarginLTRB(0, 0, 0, 0),
                      onPressed: () => Navigator.pushNamed(context, 'RegisterScreen')
                    )
                  ],
                ),
              )
            ],
          ),
        )
      ),
    );
  }

  Widget icon(ScreenScaler scaler, String icon) {
    return InkWell(
      onTap: (){},
      child: Container(
        width: scaler.getWidth(15),
        height: scaler.getWidth(10),
        padding: scaler.getPaddingLTRB(1, 0.5, 1, 0.5),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Palette.bordercolor,
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Image.asset(icon),
      ),
    );
  }
}