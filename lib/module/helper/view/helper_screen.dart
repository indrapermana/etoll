import 'package:etoll/util/palette.dart';
import 'package:etoll/widget/StandarText.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';

class HelperScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenScaler scaler = ScreenScaler()..init(context);
    return Scaffold(
      backgroundColor: Palette.bgcolor,
      body: SingleChildScrollView(
        padding: scaler.getPaddingLTRB(5, 8, 5, 0),
        child: Column(
          children: [
            Container(
              margin: scaler.getMarginLTRB(0, 0, 0, 2),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                      padding: scaler.getPaddingLTRB(1, 0.5, 1, 0.5),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(100)
                      ),
                      child: Image.asset("assets/images/icon/Icon-home.png"),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      width: scaler.getWidth(80),
                      margin: scaler.getMarginLTRB(0, 0, 2, 0),
                      child: StandarText.labelCenter("Bantuan", scaler.getTextSize(12), Palette.primary2),
                    )
                  )
                ],
              ),
            ),

            Container(
              margin: scaler.getMarginLTRB(0, 1, 0, 3),
              child: StandarText.labelCustom("Posisi di ruas Tol KM 172", scaler.getTextSize(9), color: Palette.primary1),
            ),

            Container(
              child: Column(
                children: [
                  content(scaler, "(021) 72 5423"),
                  content(scaler, "(021) 75 7676"),
                  content(scaler, "(021) 77 6767"),
                ],
              ),
            )
          ]
        )
      )
    );
  }

  Widget content(ScreenScaler scaler, String noHp) {
    return Container(
      margin: scaler.getMarginLTRB(0, 0, 0, 1),
      padding: scaler.getPaddingLTRB(3, 1, 3, 1),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Palette.shadowbox,
            blurRadius: 2.0, // soften the shadow
            spreadRadius: 1.0, //extend the shadow
            offset: Offset(
              0.0, // Move to right 10  horizontally
              1.0, // Move to bottom 5 Vertically
            ),
          )
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            margin: scaler.getMarginLTRB(0, 0, 1, 0),
            padding: scaler.getPaddingLTRB(1.5, 0.8, 1.5, 0.8),
            decoration: BoxDecoration(
              color: Palette.primary2,
              borderRadius: BorderRadius.circular(10)
            ),
            child: Image.asset("assets/images/icon/Icon-call.png"),
          ),
          Expanded(
            child: StandarText.label(noHp, scaler.getTextSize(12), Palette.primary2)
          )
        ],
      ),
    );
  }
}