import 'package:etoll/module/login/model/response_login_model.dart';
import 'package:etoll/module/login/provider/login_manager.dart';
import 'package:etoll/module/login/service/login_service.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

class RegisterProvider with ChangeNotifier {
  bool _validlogin = false;
  bool _isLoading = false;
  String _message = "";

  String _username = "";
  String _password = "";
  String _password1 = "";
  String _lokasi = "";

  bool _hidePassword = true;

  bool get validlogin => _validlogin;

  bool get isLoading => _isLoading;
  String get message => _message;

  String get username => _username;
  String get password => _password;
  String get password1 => _password1;
  String get lokasi => _lokasi;

  bool get hidePassword => _hidePassword;

  set validLogin(bool val) {
    _validlogin = val;
    notifyListeners();
  }

  set isLoading(bool val) {
    _isLoading = val;
    notifyListeners();
  }

  set message(String val) {
    _message = val;
    notifyListeners();
  }

  set username(String val) {
    _username = val;
    notifyListeners();
  }

  set password(String val) {
    _password = val;
    notifyListeners();
  }

  set password1(String val) {
    _password1 = val;
    notifyListeners();
  }

  set lokasi(String val) {
    _lokasi = val;
    notifyListeners();
  }

  set hidePassword(bool val) { 
    _hidePassword = val;
    notifyListeners();
  }

  Future submitRegiter() async {
    print('memanggil api Regiter =>' + DateFormat('hh:mm:sss').format(DateTime.now()));

    ResponseLoginModel response = await LoginService.loginAuth(_username, _password);

    String msg = '';
    bool login = true ;
    if(response.status != 200){
      login = false;
      msg = response.msg;
    }

    validLogin = login; 
    isLoading = false;
    message = msg;
    LoginManager.savetostorage(response.result);
    notifyListeners();
  }
}