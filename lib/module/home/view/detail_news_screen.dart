import 'package:etoll/util/palette.dart';
import 'package:etoll/widget/StandarText.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';

class DetailNewsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenScaler scaler = ScreenScaler()..init(context);
    return Scaffold(
      backgroundColor: Palette.bgcolor,
      body: SingleChildScrollView(
        padding: scaler.getPaddingLTRB(5, 8, 5, 0),
        child: Column(
          children: [
            Container(
              margin: scaler.getMarginLTRB(0, 0, 0, 2),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                      padding: scaler.getPaddingLTRB(1, 0.5, 1, 0.5),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(100)
                      ),
                      child: Image.asset("assets/images/icon/Icon-home.png"),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      width: scaler.getWidth(80),
                      margin: scaler.getMarginLTRB(0, 0, 2, 0),
                      child: StandarText.labelCenter("Berita", scaler.getTextSize(12), Palette.primary2),
                    )
                  )
                ],
              ),
            ),

            Container(
              margin: scaler.getMarginLTRB(0, 0, 0, 2),
              child: Image.asset("assets/images/banner/news_detail.png"),
            ),

            Container(
              width: scaler.getWidth(100),
              padding: scaler.getPaddingLTRB(1, 1, 1, 1),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Palette.shadowbox,
                    blurRadius: 2.0, // soften the shadow
                    spreadRadius: 1.0, //extend the shadow
                    offset: Offset(
                      0.0, // Move to right 10  horizontally
                      1.0, // Move to bottom 5 Vertically
                    ),
                  )
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  StandarText.labelCustom("Kecepatan Diruas KM 127", scaler.getTextSize(12)),
                  SizedBox(height: 10,),
                  StandarText.labelCustom(
                    "Kemacetan diruas km 127 diduga kareana arus dari Jakarta mengalami kemacetan di gerbang Tol Cikampek. “(Kepadatan terjadi di) pertama di SGC (Sentra Grosir Cikarang), di bundaran Grand Cikarang City, Jalan Raya Cibarusah,” ujar Kasat Lantas Polres Metro Bekasi AKBP Ojo Ruslani ketika dihubungi pada Selasa (6/10/2020) pukul 11.30 WIB.\n\nMassa buruh melakukan long march dari Jalan RE Martadinata, SGC, Jalan Raya Industri Jababeka, hingga ke Pemkab Bekasi. Saat ini buruh telah tiba di Jalan Raya Industri Jababeka.", 
                    scaler.getTextSize(10), 
                    fontWeight: FontWeight.normal,
                    textAlign: TextAlign.justify
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}