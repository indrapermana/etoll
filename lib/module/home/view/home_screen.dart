import 'package:carousel_slider/carousel_slider.dart';
import 'package:etoll/util/palette.dart';
import 'package:etoll/widget/StandarText.dart';
import 'package:etoll/widget/menu_content.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';

class HomeScreen extends StatelessWidget {
  final CarouselController _controller = CarouselController();

  @override
  Widget build(BuildContext context) {
    ScreenScaler scaler = ScreenScaler()..init(context);
    return Scaffold(
      backgroundColor: Palette.bgcolor,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: scaler.getWidth(100),
              height: scaler.getHeight(31),
              padding: scaler.getPaddingLTRB(3, 6, 3, 1),
              decoration: BoxDecoration(
                color: Palette.primary1,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20)
                )
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: scaler.getMarginLTRB(0, 0, 0, 3),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              StandarText.labelCustom("Hello!", scaler.getTextSize(9), color: Colors.white54),
                              StandarText.label("Ari Lesmana", scaler.getTextSize(12), Colors.white)
                            ],
                          ),
                        ),

                        InkWell(
                          onTap: () => Navigator.pushNamed(context, "SettingScreen"),
                          child: Container(
                            padding: scaler.getPaddingLTRB(1, 0.5, 1, 0.5),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(100)
                            ),
                            child: Image.asset("assets/images/icon/Icon-settings.png"),
                          ),
                        )
                      ],
                    ),
                  ),

                  Container(
                    padding: scaler.getPaddingLTRB(0, 1, 0, 1),
                    decoration: BoxDecoration(
                      color: Colors.white30,
                      borderRadius: BorderRadius.circular(10)
                    ),
                    child: MenuContent.menu([
                      MenuContent.menuHorizontal(
                        scaler, 
                        context, 
                        "assets/images/icon/Icon-bantuan.png", 
                        "Bantuan", 
                        () => Navigator.pushNamed(context, "HelperScreen")
                      ),
                      MenuContent.menuHorizontal(
                        scaler, 
                        context, 
                        "assets/images/icon/Icon-tambahan.png", 
                        "Tambahan", 
                        () { }
                      ),
                      MenuContent.menuHorizontal(
                        scaler, 
                        context, 
                        "assets/images/icon/Icon-rest-area.png", 
                        "Rest Area", 
                        () { }
                      ),
                      MenuContent.menuHorizontal(
                        scaler, 
                        context, 
                        "assets/images/icon/Icon-transaksi.png", 
                        "Transaksi", 
                        () => Navigator.pushNamed(context, "TransaksiScreen")
                      ),
                    ])
                  )
                ],
              ),
            ),

            Container(
              margin: scaler.getMarginLTRB(0, 1, 0, 1),
              child: CarouselSlider(
                options: CarouselOptions(
                  autoPlay: true,
                  aspectRatio: 2.0,
                  enableInfiniteScroll: false,
                  enlargeCenterPage: true,
                  
                ),
                // items: imageSliders,
                items: [
                  Container(
                    margin: scaler.getMarginLTRB(0, 0, 0, 0),
                    child: Image.asset("assets/images/banner/kartu1.png", fit: BoxFit.fill, width: scaler.getWidth(80),),
                  ),
                  Container(
                    margin: scaler.getMarginLTRB(0, 0, 0, 0),
                    child: Image.asset("assets/images/banner/kartu2.png", fit: BoxFit.fill, width: scaler.getWidth(80),),
                  ),
                ],
              ),
            ),

            Container(
              width: scaler.getWidth(100),
              margin: scaler.getMarginLTRB(4, 0, 4, 1),
              padding: scaler.getPaddingLTRB(2, 2, 2, 2),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5)
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  StandarText.labelCustom("Saldo Terpakai", scaler.getTextSize(9), fontWeight: FontWeight.normal, color: Palette.labelgrey),
                  Container(
                    margin: scaler.getMarginLTRB(0, 0, 0, 2),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        StandarText.label("Rp", scaler.getTextSize(13), Palette.primary1),
                        StandarText.label("20.000", scaler.getTextSize(18), Palette.primary1),
                      ],
                    ),
                  ),

                  StandarText.labelCustom("Anda memasuki gerbang Tol", scaler.getTextSize(9), fontWeight: FontWeight.normal, color: Palette.labelgrey),
                  Container(
                    margin: scaler.getMarginLTRB(0, 0, 0, 2),
                    child: StandarText.label("PASTEUR", scaler.getTextSize(18), Palette.primary2),
                  ),

                  StandarText.labelCustom("Gerbang Tol berikutnya:", scaler.getTextSize(9), fontWeight: FontWeight.normal, color: Palette.labelgrey),
                  Container(
                    margin: scaler.getMarginLTRB(0, 0, 0, 4),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Image.asset("assets/images/icon/Icon-arrow-up-left.png"),
                              SizedBox(width: 5,),
                              StandarText.label("Tol Moh. Toha", scaler.getTextSize(11), Palette.primary1),
                            ],
                          )
                        ),
                        VerticalDivider(color: Colors.black, width: 1,),
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Image.asset("assets/images/icon/Icon-arrow-up-left-2.png"),
                              SizedBox(width: 5,),
                              StandarText.label("Tol Cikampek", scaler.getTextSize(11), Palette.primary1),
                            ],
                          )
                        )
                      ],
                    )
                  ),

                  Container(
                    margin: scaler.getMarginLTRB(0, 0, 0, 2),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: scaler.getWidth(30),
                          padding: scaler.getPaddingAll(10),
                          decoration: BoxDecoration(
                            border: Border.all(color: Palette.bordercolor),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              StandarText.labelCustom("Km/Jam", scaler.getTextSize(9), fontWeight: FontWeight.normal, color: Palette.labelgrey),
                              Center(
                                child: StandarText.labelCustom("120", scaler.getTextSize(22), color: Palette.primary2),
                              )
                            ],
                          ),
                        ),
                        Container(
                          width: scaler.getWidth(30),
                          padding: scaler.getPaddingAll(10),
                          decoration: BoxDecoration(
                            border: Border.all(color: Palette.bordercolor),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              StandarText.labelCustom("Ruas Tol Km", scaler.getTextSize(9), fontWeight: FontWeight.normal, color: Palette.labelgrey),
                              Center(
                                child: StandarText.labelCustom("72", scaler.getTextSize(22), color: Palette.primary2),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),

                  Container(
                    width: scaler.getWidth(100),
                    margin: scaler.getMarginLTRB(0, 0, 0, 2),
                    padding: scaler.getPaddingAll(10),
                    decoration: BoxDecoration(
                      border: Border.all(color: Palette.bordercolor),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        StandarText.labelCustom("Rest Area terdekat", scaler.getTextSize(9), fontWeight: FontWeight.normal, color: Palette.labelgrey),
                        Container(
                          child: Row(
                            children: [
                              Container(
                                margin: scaler.getMarginLTRB(0, 0, 2, 0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    StandarText.label("Km", scaler.getTextSize(10), Palette.primary1),
                                    StandarText.label("45", scaler.getTextSize(18), Palette.primary1),
                                  ],
                                ),
                              ),
                              
                              Image.asset("assets/images/icon/Icon-arrow-up-left-1.png"),

                              Container(
                                width: scaler.getWidth(8),
                                height: scaler.getWidth(8),
                                margin: scaler.getMarginLTRB(2, 0, 0, 0),
                                padding: scaler.getPaddingLTRB(1.3, 0.6, 1.3, 0.6),
                                decoration: BoxDecoration(
                                  color: Palette.primary2,
                                  borderRadius: BorderRadius.circular(10)
                                ),
                                child: Image.asset("assets/images/icon/Icon-mosque.png", fit: BoxFit.fill,),
                              ),
                              Container(
                                width: scaler.getWidth(8),
                                height: scaler.getWidth(8),
                                margin: scaler.getMarginLTRB(2, 0, 0, 0),
                                padding: scaler.getPaddingLTRB(1.3, 0.6, 1.3, 0.6),
                                decoration: BoxDecoration(
                                  color: Palette.primary2,
                                  borderRadius: BorderRadius.circular(10)
                                ),
                                child: Image.asset("assets/images/icon/Icon-food.png", fit: BoxFit.fill,),
                              ),
                              Container(
                                width: scaler.getWidth(8),
                                height: scaler.getWidth(8),
                                margin: scaler.getMarginLTRB(2, 0, 0, 0),
                                padding: scaler.getPaddingLTRB(1.3, 0.6, 1.3, 0.6),
                                decoration: BoxDecoration(
                                  color: Palette.primary2,
                                  borderRadius: BorderRadius.circular(10)
                                ),
                                child: Image.asset("assets/images/icon/Icon-gas-pump.png", fit: BoxFit.fill,),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),

                  Container(
                    child: Column(
                      children: [
                        CarouselSlider(
                          carouselController: _controller,
                          options: CarouselOptions(
                            height: scaler.getHeight(20),
                            viewportFraction: 1.0,
                            enlargeCenterPage: false,
                            // autoPlay: false,
                          ),
                          items:[
                            InkWell(
                              onTap: () => Navigator.pushNamed(context, "PromosiScreen"),
                              child: Container(
                                margin: scaler.getMarginLTRB(0, 0, 0, 0),
                                child: Image.asset("assets/images/banner/banner.png", fit: BoxFit.fill, width: scaler.getWidth(80),),
                              ),
                            ),
                            InkWell(
                              onTap: (){},
                              child: Container(
                                margin: scaler.getMarginLTRB(0, 0, 0, 0),
                                child: Image.asset("assets/images/banner/portfolio_1.jpg", fit: BoxFit.fill, width: scaler.getWidth(80),),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 10,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            InkWell(
                              onTap: () => _controller.previousPage(),
                              // child: Text('←'),
                                child: Image.asset("assets/images/icon/Icon-arrow-up-left-3.png"),
                            ),
                            SizedBox(width: 20,),
                            InkWell(
                              onTap: () => _controller.nextPage(),
                              // child: Text('←'),
                                child: Image.asset("assets/images/icon/Icon-arrow-up-left-4.png"),
                            ),
                          ]
                        )
                      ],
                    )
                  ),

                  Container(
                    child: Column(
                      children: [
                        news(
                          scaler, 
                          "assets/images/banner/news1.png", 
                          "Kecepatan Durasi KM 127", 
                          "Kemacetan diruas km 127 diduga kareana arus dari Jakarta mengalami kemacetan di gerbang Tol Cikampek.", 
                          () => Navigator.pushNamed(context, "DetailNewsScreen")
                        ),
                        news(
                          scaler, 
                          "assets/images/banner/news2.png", 
                          "Gerbang Tol Cikarang Di Tutup", 
                          "Gerbang Tol Cikarang di tutup untuk sementara karena sedang menjalani pembangunan MRT Jakarta Bandung.", 
                          (){}
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget news(ScreenScaler scaler, String image, String judul, String deskripsi, GestureTapCallback onTap){
    return InkWell(
      onTap: onTap,
      child: Container(
        margin: scaler.getMarginLTRB(0, 0, 0, 1),
        padding: scaler.getPaddingLTRB(0, 1, 1, 0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Palette.shadowbox,
              blurRadius: 2.0, // soften the shadow
              spreadRadius: 1.0, //extend the shadow
              offset: Offset(
                0.0, // Move to right 10  horizontally
                1.0, // Move to bottom 5 Vertically
              ),
            )
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.asset(image),
            SizedBox(width: 5,),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  StandarText.label(judul, scaler.getTextSize(10)),
                  StandarText.labelCustom(deskripsi, scaler.getTextSize(9), fontWeight: FontWeight.normal, textAlign: TextAlign.justify),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}