import 'package:etoll/util/palette.dart';
import 'package:etoll/widget/StandarText.dart';
import 'package:etoll/widget/button_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';

class PromosiScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenScaler scaler = ScreenScaler()..init(context);
    return Scaffold(
      backgroundColor: Palette.bgcolor,
      body: SingleChildScrollView(
        padding: scaler.getPaddingLTRB(5, 8, 5, 0),
        child: Column(
          children: [
            Container(
              margin: scaler.getMarginLTRB(0, 0, 0, 2),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                      padding: scaler.getPaddingLTRB(1, 0.5, 1, 0.5),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(100)
                      ),
                      child: Image.asset("assets/images/icon/Icon-home.png"),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      width: scaler.getWidth(80),
                      margin: scaler.getMarginLTRB(0, 0, 2, 0),
                      child: StandarText.labelCenter("Promosi", scaler.getTextSize(12), Palette.primary2),
                    )
                  )
                ],
              ),
            ),

            Container(
              margin: scaler.getMarginLTRB(0, 0, 0, 2),
              child: Image.asset("assets/images/banner/banner.png"),
            ),

            Container(
              width: scaler.getWidth(100),
              margin: scaler.getMarginLTRB(0, 0, 0, 2),
              padding: scaler.getPaddingLTRB(3, 1, 3, 1),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Palette.shadowbox,
                    blurRadius: 2.0, // soften the shadow
                    spreadRadius: 1.0, //extend the shadow
                    offset: Offset(
                      0.0, // Move to right 10  horizontally
                      1.0, // Move to bottom 5 Vertically
                    ),
                  )
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  StandarText.labelCustom("Diskon", scaler.getTextSize(9), fontWeight: FontWeight.normal, color: Palette.labelgrey),
                  StandarText.labelCustom("40%", scaler.getTextSize(18), color: Palette.primary2),
                  SizedBox(height: 10,),

                  StandarText.labelCustom("Brand", scaler.getTextSize(9), fontWeight: FontWeight.normal, color: Palette.labelgrey),
                  StandarText.labelCustom("McDoanald", scaler.getTextSize(14), color: Palette.primary1),
                  SizedBox(height: 10,),
                  
                  StandarText.labelCustom("Perode", scaler.getTextSize(9), fontWeight: FontWeight.normal, color: Palette.labelgrey),
                  StandarText.labelCustom("24 Nov 20 - 1 Des 20", scaler.getTextSize(14), color: Palette.primary1),
                  SizedBox(height: 10,),

                  StandarText.labelCustom("Lokasi", scaler.getTextSize(9), fontWeight: FontWeight.normal, color: Palette.labelgrey),
                  StandarText.labelCustom("Rest Area KM 72", scaler.getTextSize(14), color: Palette.primary1),
                  SizedBox(height: 10,),
                  
                ]
              )
            ),

            ButtonColor(
              scaler: scaler,
              width: scaler.getWidth(100),
              label: "Dapatkan kode voucher", 
              lableColor: Colors.white,
              color: Palette.primary1, 
              borderRadius: BorderRadius.circular(5),
              onPressed: (){}
            )
          ]
        )
      )
    );
  }
}