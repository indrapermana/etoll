import 'dart:async';

import 'package:etoll/module/login/view/login_screen.dart';
import 'package:etoll/util/constants.dart';
import 'package:etoll/util/palette.dart';
import 'package:etoll/widget/StandarText.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 5), () => Navigator.pushReplacement(context, MaterialPageRoute(
      // builder: (context) => HomeScreen()
      builder: (context) => LoginScreen()
    )));
  }
  @override
  Widget build(BuildContext context) {
    ScreenScaler scaler = ScreenScaler()..init(context);
    return Scaffold(
      body: Container(
        width: scaler.getWidth(100),
        height: scaler.getHeight(100),
        decoration: BoxDecoration(
          color: Palette.primary1,
          // image: DecorationImage(
          //   image: AssetImage("assets/images/banner/backgroud.png"),
          //   fit: BoxFit.fill
          // )
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset("assets/images/logo/logo_putih.png")
            // Container(
            //   width: scaler.getWidth(100),
            //   padding: scaler.getPaddingLTRB(0, 0, 0, 1),
            //   child: StandarText.labelCenter(
            //     versiAplikasi, 
            //     scaler.getTextSize(8), 
            //     Palette.bgcolor
            //   ),
            // ),
          ],
        ),
      ),
      bottomSheet: Container(
        color: Palette.primary1,
        width: scaler.getWidth(100),
        height: scaler.getHeight(10),
        padding: scaler.getPaddingLTRB(0, 0, 0, 1),
        child: StandarText.labelCenter(
          versiAplikasi, 
          scaler.getTextSize(8), 
          Palette.bgcolor
        ),
      ),
    );
  }
}