import 'package:etoll/module/login/provider/login_provider.dart';
import 'package:etoll/util/palette.dart';
import 'package:etoll/widget/StandarText.dart';
import 'package:etoll/widget/button_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';
import 'package:provider/provider.dart';

class SettingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenScaler scaler = ScreenScaler()..init(context);
    return Scaffold(
      backgroundColor: Palette.bgcolor,
      body: SingleChildScrollView(
        padding: scaler.getPaddingLTRB(5, 8, 5, 0),
        child: Column(
          children: [
            Container(
              margin: scaler.getMarginLTRB(0, 0, 0, 2),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                      padding: scaler.getPaddingLTRB(1, 0.5, 1, 0.5),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(100)
                      ),
                      child: Image.asset("assets/images/icon/Icon-home.png"),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      width: scaler.getWidth(80),
                      margin: scaler.getMarginLTRB(0, 0, 2, 0),
                      child: StandarText.labelCenter("Pengaturan", scaler.getTextSize(12), Palette.primary2),
                    )
                  )
                ],
              ),
            ),

            ButtonColor(
              scaler: scaler,
              width: scaler.getWidth(100),
              label: "Ubah Profile", 
              lableColor: Colors.white,
              color: Palette.primary2, 
              borderRadius: BorderRadius.circular(5),
              onPressed: (){}
            ),

            ButtonColor(
              scaler: scaler,
              width: scaler.getWidth(100),
              label: "FAQs", 
              lableColor: Colors.white,
              color: Palette.primary2, 
              borderRadius: BorderRadius.circular(5),
              onPressed: (){}
            ),

            ButtonColor(
              scaler: scaler,
              width: scaler.getWidth(100),
              label: "Legal", 
              lableColor: Colors.white,
              color: Palette.primary2, 
              borderRadius: BorderRadius.circular(5),
              onPressed: (){}
            ),

            ButtonColor(
              scaler: scaler,
              width: scaler.getWidth(100),
              label: "About", 
              lableColor: Colors.white,
              color: Palette.primary2, 
              borderRadius: BorderRadius.circular(5),
              onPressed: (){}
            ),

            ButtonColor(
              scaler: scaler,
              width: scaler.getWidth(100),
              label: "Keluar", 
              lableColor: Colors.white,
              color: Palette.primary1, 
              borderRadius: BorderRadius.circular(5),
              onPressed: () {
                LoginProvider load = Provider.of<LoginProvider>(context, listen: false);
                load.validLogin = false;
                Navigator.pop(context);
              }
            ),
          ]
        )
      )
    );
  }
}