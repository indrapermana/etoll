import 'package:etoll/util/palette.dart';
import 'package:etoll/widget/StandarText.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';

class TransaksiScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenScaler scaler = ScreenScaler()..init(context);
    return Scaffold(
      backgroundColor: Palette.bgcolor,
      body: SingleChildScrollView(
        padding: scaler.getPaddingLTRB(5, 8, 5, 0),
        child: Column(
          children: [
            Container(
              margin: scaler.getMarginLTRB(0, 0, 0, 2),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                      padding: scaler.getPaddingLTRB(1, 0.5, 1, 0.5),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(100)
                      ),
                      child: Image.asset("assets/images/icon/Icon-home.png"),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      width: scaler.getWidth(80),
                      margin: scaler.getMarginLTRB(0, 0, 2, 0),
                      child: StandarText.labelCenter("Transaksi", scaler.getTextSize(12), Palette.primary2),
                    )
                  )
                ],
              ),
            ),

            Container(
              child: Column(
                children: [
                  content(scaler, "24/11/20 19:36:12", "PASTEUR", "20.000"),
                  content(scaler, "24/11/20 19:36:12", "MOH. TOHA", "15.000"),
                  content(scaler, "24/11/20 19:36:12", "BUAH BATU", "45.000"),
                ],
              ),
            )
          ]
        )
      )
    );
  }

  Widget content(ScreenScaler scaler, String tanggal, String tol, String nominal) {
    return Container(
      margin: scaler.getMarginLTRB(0, 0, 0, 1),
      padding: scaler.getPaddingLTRB(3, 1, 3, 1),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Palette.shadowbox,
            blurRadius: 2.0, // soften the shadow
            spreadRadius: 1.0, //extend the shadow
            offset: Offset(
              0.0, // Move to right 10  horizontally
              1.0, // Move to bottom 5 Vertically
            ),
          )
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          StandarText.labelCustom(tanggal, scaler.getTextSize(10), fontWeight: FontWeight.normal),
          SizedBox(height: 10,),

          StandarText.labelCustom("Transaksi gerbang tol", scaler.getTextSize(9), fontWeight: FontWeight.normal, color: Palette.labelgrey),
          StandarText.labelCustom(tol, scaler.getTextSize(14), color: Palette.primary2),
          SizedBox(height: 10,),

          StandarText.labelCustom("Biaya transaksi", scaler.getTextSize(9), fontWeight: FontWeight.normal, color: Palette.labelgrey),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      StandarText.label("Rp", scaler.getTextSize(11), Palette.primary1),
                      StandarText.label(nominal, scaler.getTextSize(14), Palette.primary1),
                    ],
                  ),
                ),
                InkWell(
                  onTap: (){},
                  child: Container(
                    padding: scaler.getPaddingLTRB(1.5, 0.8, 1.5, 0.8),
                    decoration: BoxDecoration(
                      color: Palette.primary2,
                      borderRadius: BorderRadius.circular(100)
                    ),
                    child: Image.asset("assets/images/icon/Icon-copy.png", width: scaler.getWidth(3), height: scaler.getWidth(3),),
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 10,),
        ],
      ),
    );
  }
}